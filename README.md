CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
This module adds the "Search API Solr: Boosted Keyword" field type, widget and formatter.
This field type can be used to add variably boosted keywords to indexed content.
Each item of this field type contains a keyword and it's boost level.

Alters Search API Solr indexing so the keywords are repeated x number of times in the document (x being the selected
boost level for that keyword).

Alters Search API Solr queries to include a termfreq boost function for each indexed keyword per given key.

REQUIREMENTS
------------
* Search API (https://www.drupal.org/project/search_api)
* Search API Solr (https://www.drupal.org/project/search_api_solr)

INSTALLATION
------------
* Install the module and required modules.

CONFIGURATION
-------------
* Add a "Search API Solr: Boosted Keyword" field to the entity types / bundles that are
going to use the boosted keyword functionality.
* Make sure there is a Search API server configured with the Solr backend.
* Make sure there is an index on the Search API Solr server.
* Index at least 1 "Search API Solr: Boosted Keyword" field (fulltext is advised as it allows
boosting the field itself, determining the general importance of the boosted keywords).
* Set up any form of Search API query (e.g. a Search API Index view) ordered by relevance.

MAINTAINERS
-----------
Current maintainers:
* Laurens Van Damme (L_VanDamme) - https://www.drupal.org/u/l_vandamme
* Alex Kuzava (nginex) - https://www.drupal.org/u/nginex
