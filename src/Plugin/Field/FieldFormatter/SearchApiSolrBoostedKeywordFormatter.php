<?php

namespace Drupal\search_api_solr_boosted_keyword\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Implementation of the 'search_api_solr_boosted_keyword_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "search_api_solr_boosted_keyword_formatter",
 *   module = "search_api_solr_boosted_keyword",
 *   label = @Translation("Search API Solr: Boosted Keyword"),
 *   field_types = {
 *     "search_api_solr_boosted_keyword"
 *   }
 * )
 */
class SearchApiSolrBoostedKeywordFormatter extends FormatterBase {

  /**
   * Displays the Boosted Keyword items.
   *
   * @inheritDoc
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // Print each keyword as a simple string with its boost level.
    $boostLevelLabel = $this->t('Boost level');
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'inline_template',
        '#template' => '{{ value|nl2br }} (' . $boostLevelLabel . ': ' . $item->boost . ')',
        '#context' => ['value' => $item->value],
      ];
    }

    return $elements;
  }

}
