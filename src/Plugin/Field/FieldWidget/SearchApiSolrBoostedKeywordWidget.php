<?php

namespace Drupal\search_api_solr_boosted_keyword\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Default widget for search_api_solr_boosted_keyword field.
 *
 * @FieldWidget(
 *   id = "search_api_solr_boosted_keyword_widget",
 *   label = @Translation("Search API Solr: Boosted Keyword"),
 *   field_types = {
 *     "search_api_solr_boosted_keyword"
 *   }
 * )
 */
class SearchApiSolrBoostedKeywordWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // Remove the fieldset if multiple values are allowed.
    $cardinality = $items->getFieldDefinition()->getFieldStorageDefinition()->getCardinality();
    $element['#type'] = 'fieldset';
    if ($cardinality != 1) {
      $element['#type'] = 'container';
      $element['#attributes']['style'] = 'display:flex;';
    }

    // Add the value and boost inputs.
    $element['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Keyword'),
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
      '#required' => $element['#required'],
      '#attributes' => ['style' => 'width: calc(100% - 20px);'],
    ];
    $element['boost'] = [
      '#type' => 'number',
      '#title' => $this->t('Boost level'),
      '#min' => 1,
      '#max' => 20,
      '#step' => 1,
      '#default_value' => isset($items[$delta]->boost) ? $items[$delta]->boost : 1,
      '#required' => $element['#required'],
    ];

    return $element;
  }

}
