<?php

namespace Drupal\search_api_solr_boosted_keyword\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'search_api_solr_boosted_keyword' field type.
 *
 * @FieldType(
 *   id = "search_api_solr_boosted_keyword",
 *   label = @Translation("Search API Solr: Boosted Keyword"),
 *   description = @Translation("This field stores a keyword with a boost level, to be used with Search API Solr."),
 *   default_widget = "search_api_solr_boosted_keyword_widget",
 *   default_formatter = "search_api_solr_boosted_keyword_formatter"
 * )
 */
class SearchApiSolrBoostedKeyword extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Keyword'))
      ->setRequired(TRUE);
    $properties['boost'] = DataDefinition::create('integer')
      ->setLabel(t('Boost level'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'boost' => [
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

}
