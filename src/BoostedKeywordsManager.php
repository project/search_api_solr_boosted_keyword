<?php

namespace Drupal\search_api_solr_boosted_keyword;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManager;

/**
 * Defines a service to manage boosted keywords.
 *
 * @package Drupal\search_api_solr_boosted_keyword
 */
class BoostedKeywordsManager {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * BoostedKeywordsManager constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(Connection $database, EntityFieldManager $entity_field_manager) {
    $this->database = $database;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Get usages of boosted keywords.
   *
   * @param string $return_type
   *   Whether to return list of bundles or field names where keywords used.
   * @param string $entity_type
   *   The entity type to filter usages.
   *
   * @return array
   *   List of fields or bundles where keywords used based on the return type.
   */
  public function whereUsed($return_type, $entity_type = 'node') {
    $usages = $this->entityFieldManager->getFieldMapByFieldType('search_api_solr_boosted_keyword');

    if (!isset($usages[$entity_type])) {
      return [];
    }

    switch ($return_type) {
      case 'bundle':
        $bundles = [];

        foreach ($usages[$entity_type] as $info) {
          $bundles = array_merge($bundles, $info['bundles']);
        }

        return array_values(array_unique($bundles));

      case 'field_name':
        return array_keys($usages[$entity_type]);
    }

    return [];
  }

  /**
   * Prepare child query to be used in the main query with union feature.
   *
   * @param string $field_name
   *   The name of the boosted keywords field.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   *   The select sql query.
   */
  protected function prepareQueryForSingleKeywordField($field_name, array $filters) {
    $query = $this->database->select("node__{$field_name}", 'k');
    $query->join('node_field_data', 'n', 'k.entity_id = n.nid AND k.langcode = n.langcode');

    $query->addField('k', "{$field_name}_value", 'keyword');

    // Group all pages that use the keyword by contacting data grouped data.
    $query->addExpression("GROUP_CONCAT(CONCAT_WS(',', n.nid, n.changed, n.langcode, k.{$field_name}_boost) ORDER BY n.nid ASC SEPARATOR '|')", 'pages');

    // Filter by content type.
    if (isset($filters['type'])) {
      $query->condition('k.bundle', $filters['type']);
    }

    // Filter by language.
    if (isset($filters['language'])) {
      $query->condition('k.langcode', $filters['language']);
    }

    // Filter by status.
    if (isset($filters['status']) && $filters['status'] !== 'all') {
      $query->condition('n.status', $filters['status']);
    }

    // Group results by the keywords.
    $query->groupBy('keyword');

    return $query;
  }

  /**
   * Fetch list of keywords with additional info.
   *
   * @param array $filters
   *   List of filters to filter results. Allowed filters are:
   *   - type: the bundle of the node.
   *   - language: the language of the node.
   *   - status: the status of the node.
   * @param int $limit
   *   The pager limit.
   *
   * @return array
   *   The array with keywords from the database.
   */
  public function fetchAllKeywords(array $filters, $limit) {
    $fields = $this->whereUsed('field_name');

    // No usages - no results.
    if (!$fields) {
      return [];
    }

    // Build main query with first field in the usages.
    $field_name = array_shift($fields);
    $query = $this->prepareQueryForSingleKeywordField($field_name, $filters);

    // Go through the rest of the fields and use union to combine results.
    foreach ($fields as $field_name) {
      $query_child = $this->prepareQueryForSingleKeywordField($field_name, $filters);
      $query->union($query_child, 'UNION ALL');
    }

    // Sort alphabetically by keyword.
    $query->orderBy('keyword');

    if ($limit > 0) {
      $query = $query->extend('\Drupal\Core\Database\Query\PagerSelectExtender')->limit($limit);
    }

    return $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
  }

}
