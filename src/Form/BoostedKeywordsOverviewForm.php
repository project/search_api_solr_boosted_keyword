<?php

namespace Drupal\search_api_solr_boosted_keyword\Form;

use Drupal\search_api_solr_boosted_keyword\BoostedKeywordsManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\node\NodeTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form to filter and export boosted keywords.
 *
 * @package Drupal\search_api_solr_boosted_keyword\Form
 */
class BoostedKeywordsOverviewForm extends FormBase {

  /**
   * The boosted keywords manager.
   *
   * @var \Drupal\search_api_solr_boosted_keyword\BoostedKeywordsManager
   */
  protected $keywordsManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The list of available filters.
   *
   * @var string[]
   */
  protected $availableFilters = [
    'type',
    'language',
    'status',
  ];

  /**
   * BoostedKeywordsOverviewForm constructor.
   *
   * @param \Drupal\search_api_solr_boosted_keyword\BoostedKeywordsManager $keywords_manager
   *   The boosted keywords manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(BoostedKeywordsManager $keywords_manager, LanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->keywordsManager = $keywords_manager;
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('search_api_solr_boosted_keyword.keywords_manager'),
      $container->get('language_manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'search_api_solr_boosted_keyword_overview';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $query = $this->getRequest()->query->all();

    // Build a filters for the overview.
    $form['filters'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filters'),
      '#attributes' => ['class' => ['form--inline']],
    ];

    // Get list of node bundles where keywords used.
    $bundles_usage = $this->keywordsManager->whereUsed('bundle');

    // Warning user that there are no usages.
    if (!$bundles_usage) {
      $this->messenger()->addWarning($this->t('Currently there is no boosted keywords field added in your content types.'));
    }

    $node_types = $this->entityTypeManager->getStorage('node_type')
      ->loadMultiple($bundles_usage);

    $form['filters']['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Content type'),
      '#options' => array_map(function (NodeTypeInterface $node_type) {
        return $node_type->label();
      }, $node_types),
      '#empty_option' => $this->t('- Any -'),
      '#default_value' => isset($query['type']) && in_array($query['type'], $bundles_usage) ? $query['type'] : NULL,
    ];

    $languages = $this->languageManager->getLanguages();
    $form['filters']['language'] = [
      '#type' => 'select',
      '#title' => $this->t('Language'),
      '#options' => array_map(function (LanguageInterface $language) {
        return $language->getName();
      }, $languages),
      '#empty_option' => $this->t('- Any -'),
      '#default_value' => isset($query['language']) && isset($languages[$query['language']]) ? $query['language'] : NULL,
    ];

    $form['filters']['status'] = [
      '#type' => 'select',
      '#title' => $this->t('Status'),
      '#options' => [
        NodeInterface::PUBLISHED => $this->t('Published'),
        NodeInterface::NOT_PUBLISHED => $this->t('Unpublished'),
      ],
      '#empty_option' => $this->t('- Any -'),
      '#empty_value' => 'all',
      '#default_value' => $query['status'] ?? NodeInterface::PUBLISHED,
    ];

    $form['filters']['actions'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-actions'],
      ],
      'filter' => [
        '#type' => 'submit',
        '#value' => $this->t('Filter'),
        '#button_type' => 'primary',
        '#name' => 'filter',
      ],
      'reset' => [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#name' => 'reset',
      ],
    ];

    // Build a table with keywords overview.
    $headers = [
      'keyword' => $this->t('Keyword'),
      'page' => $this->t('Page'),
      'boost' => $this->t('Boost'),
      'last_update' => $this->t('Last update'),
    ];

    $rows = [];
    $filters = [];

    foreach ($this->availableFilters as $available_filter) {
      if (isset($form['filters'][$available_filter]['#default_value'])) {
        $filters[$available_filter] = $form['filters'][$available_filter]['#default_value'];
      }
    }

    $results = $this->keywordsManager->fetchAllKeywords($filters, 100);

    foreach ($results as $result) {
      $rows[$result['keyword']] = [
        'keyword' => $result['keyword'],
      ];

      // Extract pages.
      $pages = explode('|', $result['pages']);
      $keyword_values = [];

      foreach ($pages as $page) {
        [$nid, $timestamp, $langcode, $boost] = explode(',', $page);

        $url = Url::fromRoute('entity.node.canonical', ['node' => $nid], [
          'absolute' => TRUE,
          'language' => $languages[$langcode],
          'attributes' => [
            'target' => '_blank',
          ],
        ]);

        $keyword_values['page'][] = Link::fromTextAndUrl($url->toString(), $url);
        $keyword_values['boost'][] = $boost;
        $keyword_values['last_update'][] = date('d-m-Y', $timestamp);
      }

      // Build grouped rows into lists per a keyword.
      foreach (['page', 'boost', 'last_update'] as $key) {
        $rows[$result['keyword']][$key]['data'] = [
          '#theme' => 'item_list',
          '#list_type' => 'ol',
          '#items' => $keyword_values[$key],
          '#attributes' => [
            'style' => 'list-style: none;',
          ],
        ];
      }
    }

    $form['actions'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Actions'),
      '#description' => $this->t('Actions are applied for all filtered results.'),
    ];

    $form['actions']['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['actions']['export'] = [
      '#type' => 'submit',
      '#name' => 'csv_export',
      '#value' => $this->t('Export to CSV'),
      '#disabled' => count($results) < 1,
    ];

    $form['keywords'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => $this->t('No boosted keywords found'),
    ];

    $form['pager'] = [
      '#type' => 'pager',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = [];

    foreach ($this->availableFilters as $available_filter) {
      $value = $form_state->getValue($available_filter);
      if ($form_state->hasValue($available_filter) && $value !== '') {
        $query[$available_filter] = $value;
      }
    }

    $button = $form_state->getTriggeringElement();

    switch ($button['#name']) {
      case 'filter':
      case 'reset':
        $url = Url::fromRoute('search_api_solr_boosted_keyword.keywords_overview');

        // Apply filters.
        if ($button['#name'] === 'filter') {
          $url->setOption('query', $query);
        }

        $form_state->setRedirectUrl($url);
        break;

      case 'csv_export':
        $languages = $this->languageManager->getLanguages();
        $filename = 'search_boosted_keywords_export_' . date('Y_m_d_H_i') . '.csv';
        $destination = 'temporary://' . $filename;

        /** @var \Drupal\file\FileInterface $file */
        $file = $this->entityTypeManager->getStorage('file')
          ->create([
            'uid' => $this->currentUser()->id(),
            'filename' => $filename,
            'uri' => $destination,
            'filemime' => 'application/csv',
            'status' => 0,
          ]);

        // Save the file temporary, exported file can then be removed on cron.
        $file->save();

        $headers = [
          'Keyword',
          'Page',
          'Boost',
          'Last update',
        ];

        $handle = fopen($destination, 'w');
        fputcsv($handle, $headers, ';');

        // Set limit 0 to avoid using of the pager.
        $rows = $this->keywordsManager->fetchAllKeywords($query, 0);

        // Put rows in the CSV file.
        foreach ($rows as $row) {
          // Extract pages.
          $pages = explode('|', $row['pages']);

          foreach ($pages as $page) {
            [$nid, $timestamp, $langcode, $boost] = explode(',', $page);

            $url = Url::fromRoute('entity.node.canonical', ['node' => $nid], [
              'absolute' => TRUE,
              'language' => $languages[$langcode],
            ]);

            $fields = [
              'Keyword' => $row['keyword'],
              'Page' => $url->toString(),
              'Boost' => $boost,
              'Last update' => date('d-m-Y', $timestamp),
            ];

            fputcsv($handle, $fields, ';');
          }
        }
        fclose($handle);

        // Display a message with the link to download generated file.
        $file_url = $file->createFileUrl();
        $url = Url::fromUserInput($file_url, [
          'attributes' => ['download' => $filename],
          'absolute' => TRUE,
        ]);
        $link = Link::fromTextAndUrl($this->t('Click here'), $url);
        $message = $this->t('@count boosted keywords have been successfully exported. @link to download the file.', [
          '@count' => count($rows),
          '@link' => $link->toString(),
        ]);

        $this->messenger()->addStatus($message);
        break;
    }
  }

}
